# **_Milestone 1: Individual Report_**

## **Sam Halligan_**

### **Research Findings**

### **Functions**
> The main use for the Facebook API is to quickly gather user information by reading the information which they have stored on their profile, it also has a use as a "Social Plugin" to view what select people have commented or liked. The Facebook API uses a "Login token" to ask the user for permission to use their infromation according to the sites privacy policy.     

https://developers.facebook.com/docs/javascript/reference/FB.api/
https://developers.facebook.com/docs/marketing-apis 
### **Aesthetics**
> The aesthetics of most of the Facebook API are stored on Facebook itself so it utilises the fonts provided by them, outside of this we will be able to choose the fonts and style which we want and will do so in a simple yet effective style which matches the current style of Facebook. 

https://www.sitepoint.com/fonts-colors-used-facebook-twitter-google/
### **Components**
> A big Component which we will be using is the get function on user data it gives you a response window which shows all the information that your app has permission to pull. The app itself has many individual components to it including; webpages, login prompts, the login page and the home page all of which work together to make the app function as a whole.  

https://developers.facebook.com/docs/javascript/examples
### **Updating Data**
> To upload data you need to make a "Upload Session" and you are provided with a session ID through this you can start uploading file data. A GET call can be used to check the upload status of the data file which you are currently uploading. 

https://developers.facebook.com/docs/graph-api/resumable-upload-api/ 